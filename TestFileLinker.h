#ifndef FOO_DOT_H    /* This is an "include guard" */
#define FOO_DOT_H    /* prevents the file from being included twice. */
                     /* Including a header file twice causes all kinds */
                     /* of interesting problems.*/

/**
 * This is a function declaration.
 * It tells the compiler that the function exists somewhere.
 */
//int **MatrixMultiplication(int first_row,int first_col,int second_row,int second_col,int** matrix1,int** matrix2);
void **MM_ijk(int first_row,int first_col,int second_row,int second_col,int** matrix1, int** matrix2,int** result);
void **MM_jik(int first_row,int first_col,int second_row,int second_col,int** matrix1, int** matrix2,int** result);
void **MM_kij(int first_row,int first_col,int second_row,int second_col,int** matrix1, int** matrix2,int** result);
void **MM_ikj(int first_row,int first_col,int second_row,int second_col,int** matrix1, int** matrix2,int** result);
void **MM_jki(int first_row,int first_col,int second_row,int second_col,int** matrix1, int** matrix2,int** result);
void **MM_kji(int first_row,int first_col,int second_row,int second_col,int** matrix1, int** matrix2,int** result);



#endif /* FOO_DOT_H */