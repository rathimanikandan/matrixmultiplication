#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include "MM.c"
#include "TestLinker.h"
int main(int argc, char  **argv)         
{
	FILE *fptr,*fptr1;

    char filename[100];
    strcpy(filename,argv[1]);
    printf("------------------------------------------------------------------------------"); 
	printf("\n%s   \n\n",argv[1]);
    char ch;
    char c[10] , empty[1];
    fptr = fopen(filename, "r");
    int i,t=0,num=0;
    int arr[10];
    
	// Reading the dimension of the matrix form the file 
	for(t=0;t<4;)
	{
		ch=fgetc(fptr);
			if(isspace(ch))
			{
				arr[t]=num;
				num=0;
				t++;
				
			}
			else
			{
				
				if(ch == '-'){
					printf("Row or Column value should be Greater than 0");
					exit(0);
				}
				else{
				num =(num*10) +(ch-'0');
				}
				
			}
	}
	
	int r1=arr[0],c1=arr[1],r2=arr[2],c2=arr[3];
	 if(r1<0==1||r2<0==1||c1<0==1||c2<0==1){
     	printf("\nTestcase %s Failed!",filename);
	 }
	// checking whether row or column values have 0 as value
	assert(r1>0);
	assert(c1>0);
	assert(r2>0);
	assert(c2>0); 
	//	Verifying the dimensions of matrices whether valid for matrix multiplication or not
	if(c1 != r2){
		printf("Different Column1 and row2 values!!");
		exit(0);
	}
	

 	//	Allocating memory for matrix1,matrix2,expected_result matrices
 	int**  matrix1 ;
	matrix1 = (int **) malloc(sizeof(int *) * r1);
	for(i=0; i<r1; i++)  
 		matrix1[i] = (int *)malloc(sizeof(int) * c1);
 	
 	int**  matrix2 ;
	matrix2 = (int **) malloc(sizeof(int *) * r2);
	for(i=0; i<r2; i++)  
 		matrix2[i] = (int *)malloc(sizeof(int) * c2);
 		
 	int**  expected_result ;
	expected_result = (int **) malloc(sizeof(int *) * r1);
	for(i=0; i<r1; i++)  
 		expected_result[i] = (int *)malloc(sizeof(int) * c2);


	//	Reading Matrix1 values one by one from text file
	num=0;
	ch=fgetc(fptr);
	int x , y , k;
	for( x=0;x<r1;x++)
	{
		for( y=0;y<c1;)
		{
		
			ch=fgetc(fptr);
			if(isspace(ch))
			{
				num = atoi(c);
				
				matrixA[x][y]=num;
				strcpy(c , empty);
				num=0;
				y++;
			
			}
			else
			{
				strncat(c , &ch , 1);
			}
		}
	}
	
	printf("\n");

	//	Reading matrix2 values one by one from text file

	num=0;
	ch=fgetc(fptr);
	for( x=0;x<r2;x++)
	{
		for( y=0;y<c2;)
		{
			ch=fgetc(fptr);
			
			if(isspace(ch))
			{
				num = atoi(c);
				
				matrixB[x][y]=num;
				strcpy(c , empty);
				num=0;
				y++;
			
			}
			else
			{
				strncat(c , &ch , 1);
			}
		}
		
	}

	//	Reading values for resultant_matrix from the test file
	
	num=0;
	ch=fgetc(fptr);
	for( x=0;x<r1;x++)
	{
		for( y=0;y<c2;)
		{
		ch=fgetc(fptr);
		if(isspace(ch))
			{
				num = atoi(c);
				
				expected_result[x][y]=num;
				strcpy(c , empty);
				num=0;
				y++;
			
			}
			else
			{
				strncat(c , &ch , 1);
			}
		}
	}

	//	creating a matrix to store actual_result obtained from the MM code
	
	int** actual_result;
	actual_result = (int **) malloc(sizeof(int *) * r1);
	for(i=0; i<r1; i++)  
 		actual_result[i] = (int *)malloc(sizeof(int) * c2); 
	//  initializing all the values of the matrix to be 0 at the beginning
	for(x=0;x<r1;x++)
	{
		for(y=0;y<c2;y++)
		{
			actual_result[x][y]=0;
		}
	}


	clock_t begin=clock();							//clock started
	//	MM function is called 
	MM_ikj(r1,c1,r2,c2,matrix1,matrix2,actual_result);			
	clock_t end= clock();	                    	// clock ended											
	double time_spent =0.0;
	time_spent += (double)(end - begin) * 1000.0  / CLOCKS_PER_SEC;			//	time_spent is calculated
	printf("\nTime elapsed: %.2f ms", time_spent);
    printf("\n");
	
	
	//	checking whether each and every values of expected_result and actual_result matrices matches 
	int j;
	for( i=0;i<r1;i++)
	{
		for( j=0;j<c2;j++)
		{
			if(expected_result[i][j]!=actual_result[i][j] )
			{
					printf("\nTestcase %s Failed!!\n",filename);
				return 0; 
			}
		}
				
	}
		printf("\nTestcase %s Passed!\n",filename);
}