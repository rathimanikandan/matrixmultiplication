Workflow
1. Tester code takes input from text file, converts into integer 
2. Row/col value for both matrices and inputs are obtained.
3. Function is called and that function returns the resultant matrix.
4. Each and every value of the actual output is checked with the expected output
5. if it matches, assertion will be successful, else it will fail

Test Cases
1. Test1.txt -->  1x1 and 1x1
2. Test2.txt -->  1x2 and 2x3
3. Test3.txt -->  3x2 and 2x1
4. Test4.txt -->  2x1 and 1x2
5. Test5.txt -->  1x2 and 2x1
6. Test6.txt -->  1x1 and 1x2
7. Test7.txt -->  0x0 and 0x0
8. Test8.txt -->  2x0 and 0x2
9. Test9.txt --> matrix with negative values

Run command:
1.compile and run "tester.c"
2.The code will test for the TestFile which is given in 11th line in "tester.c"
3.To check for various test cases, the TestFile name should be modified with the files in my project list and the "tester.c" should be compiled and run again.


GOOGLE SHEET LINK : 

https://docs.google.com/spreadsheets/d/1Gi2mY3rao8JqS7d641wqaFLdyBm5Ab6pH7N965G5-RU/edit#gid=1954088513
