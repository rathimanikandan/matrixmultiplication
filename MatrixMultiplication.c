
#include<assert.h>
#include "TestFileLinker.h"  

void MM_ijk(int first_row,int first_col,int second_row,int second_col,int** matrix1, int** matrix2,int** result){
// This function takes two matrices as inputs along with their row and column count
// The multiplication is performed for various cases and output is stored in 'result' matrix
// The 'result' matrix now has the resultant values    
    
        int i , j , k;
 	for(i=0; i < first_row; i++) 
	{ 
		for(j=0; j < second_col; j++) 
		{ 
	 		for(k=0; k<first_col; k++)  
				result[i][j] += matrix1[i][k] * matrix2[k][j]; 
		} 
	} 
    
}

void MM_jik(int first_row,int first_col,int second_row,int second_col,int** matrix1, int** matrix2,int** result){
// This function takes two matrices as inputs along with their row and column count
// The multiplication is performed for various cases and output is stored in 'result' matrix
// The 'result' matrix now has the resultant values
     
      int i , j , k;
 	for(j=0; j < first_row; j++) 
	{ 
		for(i=0; i < second_col; i++) 
		{ 
	 		for(k=0; k<first_col; k++)  
				result[j][i] += matrix1[j][k] * matrix2[k][i]; 
		} 
	} 
  
}



void MM_kij(int first_row,int first_col,int second_row,int second_col,int** matrix1, int** matrix2,int** result){
// This function takes two matrices as inputs along with their row and column count
// The multiplication is performed for various cases and output is stored in 'result' matrix
// The 'result' matrix now has the resultant values
     int i , j , k;
 	for(k=0; k < first_row; k++) 
	{ 
		for(i=0; i < second_col; i++) 
		{ 
	 		for(j=0; j<first_col; j++)  
				result[k][i] += matrix1[k][j] * matrix2[j][i]; 
		} 
	} 
}


void MM_ikj(int first_row,int first_col,int second_row,int second_col,int** matrix1, int** matrix2,int** result){
// This function takes two matrices as inputs along with their row and column count
// The multiplication is performed for various cases and output is stored in 'result' matrix
// The 'result' matrix now has the resultant values
      int i , j , k;
 	for(i=0; i < first_row; i++) 
	{ 
		for(k=0; k < second_col; k++) 
		{ 
	 		for(j=0; j<first_col; j++)  
				result[i][k] += matrix1[i][j] * matrix2[j][k]; 
		} 
	} 
}


void MM_jki(int first_row,int first_col,int second_row,int second_col,int** matrix1, int** matrix2,int** result){
// This function takes two matrices as inputs along with their row and column count
// The multiplication is performed for various cases and output is stored in 'result' matrix
// The 'result' matrix now has the resultant values
     int i , j , k;
 	for(j=0; j < first_row; j++) 
	{ 
		for(k=0; k < second_col; k++) 
		{ 
	 		for(i=0; i<first_col; i++)  
				result[j][k] += matrix1[j][i] * matrix2[i][k]; 
		} 
	} 
}


void MM_kji(int first_row,int first_col,int second_row,int second_col,int** matrix1, int** matrix2,int** result){
// This function takes two matrices as inputs along with their row and column count
// The multiplication is performed for various cases and output is stored in 'result' matrix
// The 'result' matrix now has the resultant values
 int i , j , k;
 	for(k=0; k < first_row; k++) 
	{ 
		for(j=0; j < second_col; j++) 
		{ 
	 		for(i=0; i<first_col; i++)  
				result[k][j] += matrix1[k][i] * matrix2[i][j]; 
		} 
	}    

}


#include<assert.h>
#include "TestFileLinker.h"  

void MM_ijk(int first_row,int first_col,int second_row,int second_col,int** matrix1, int** matrix2,int** result){
// This function takes two matrices as inputs along with their row and column count
// The multiplication is performed for various cases and output is stored in 'result' matrix
// The 'result' matrix now has the resultant values    
    
        int i , j , k;
 	for(i=0; i < first_row; i++) 
	{ 
		for(j=0; j < second_col; j++) 
		{ 
	 		for(k=0; k<first_col; k++)  
				result[i][j] += matrix1[i][k] * matrix2[k][j]; 
		} 
	} 
    
}

void MM_jik(int first_row,int first_col,int second_row,int second_col,int** matrix1, int** matrix2,int** result){
// This function takes two matrices as inputs along with their row and column count
// The multiplication is performed for various cases and output is stored in 'result' matrix
// The 'result' matrix now has the resultant values
     
      int i , j , k;
 	for(j=0; j < first_row; j++) 
	{ 
		for(i=0; i < second_col; i++) 
		{ 
	 		for(k=0; k<first_col; k++)  
				result[j][i] += matrix1[j][k] * matrix2[k][i]; 
		} 
	} 
  
}



void MM_kij(int first_row,int first_col,int second_row,int second_col,int** matrix1, int** matrix2,int** result){
// This function takes two matrices as inputs along with their row and column count
// The multiplication is performed for various cases and output is stored in 'result' matrix
// The 'result' matrix now has the resultant values
     int i , j , k;
 	for(k=0; k < first_row; k++) 
	{ 
		for(i=0; i < second_col; i++) 
		{ 
	 		for(j=0; j<first_col; j++)  
				result[k][i] += matrix1[k][j] * matrix2[j][i]; 
		} 
	} 
}


void MM_ikj(int first_row,int first_col,int second_row,int second_col,int** matrix1, int** matrix2,int** result){
// This function takes two matrices as inputs along with their row and column count
// The multiplication is performed for various cases and output is stored in 'result' matrix
// The 'result' matrix now has the resultant values
      int i , j , k;
 	for(i=0; i < first_row; i++) 
	{ 
		for(k=0; k < second_col; k++) 
		{ 
	 		for(j=0; j<first_col; j++)  
				result[i][k] += matrix1[i][j] * matrix2[j][k]; 
		} 
	} 
}


void MM_jki(int first_row,int first_col,int second_row,int second_col,int** matrix1, int** matrix2,int** result){
// This function takes two matrices as inputs along with their row and column count
// The multiplication is performed for various cases and output is stored in 'result' matrix
// The 'result' matrix now has the resultant values
     int i , j , k;
 	for(j=0; j < first_row; j++) 
	{ 
		for(k=0; k < second_col; k++) 
		{ 
	 		for(i=0; i<first_col; i++)  
				result[j][k] += matrix1[j][i] * matrix2[i][k]; 
		} 
	} 
}


void MM_kji(int first_row,int first_col,int second_row,int second_col,int** matrix1, int** matrix2,int** result){
// This function takes two matrices as inputs along with their row and column count
// The multiplication is performed for various cases and output is stored in 'result' matrix
// The 'result' matrix now has the resultant values
 int i , j , k;
 	for(k=0; k < first_row; k++) 
	{ 
		for(j=0; j < second_col; j++) 
		{ 
	 		for(i=0; i<first_col; i++)  
				result[k][j] += matrix1[k][i] * matrix2[i][j]; 
		} 
	}    

}

